use amethyst::{
    assets::{AssetStorage, Loader, Handle},
    core::transform::Transform,
    ecs::{Component, DenseVecStorage},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
};

pub const MAP_HEIGHT: f32 = 100.0;
pub const MAP_WIDTH: f32 = 100.0;

// pub const SQUARE_SIZE: f32 = 32.0;

pub const PADDLE_HEIGHT: f32 = 16.0;
pub const PADDLE_WIDTH: f32 = 4.0;

pub struct Game;

impl SimpleState for Game {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        let sprite_sheet_handle = load_sprite_sheet(world);
        
        world.register::<Paddle>(); // this will be deleted later, this is a workaround Amethyst has before I implement systems
        // world.register::<Square>();

        initialise_paddles(world, sprite_sheet_handle);
        // initialize_squares(world, sprite_sheet_handle);
        initialize_camera(world);
    }
}

fn initialize_camera(world: &mut World) {
    // Setup camera in a way that our screen covers whole arena and (0, 0) is in the bottom left.
    let mut transform = Transform::default();
    transform.set_translation_xyz(MAP_WIDTH * 0.5, MAP_HEIGHT * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(MAP_WIDTH, MAP_HEIGHT))
        .with(transform)
        .build();
}

#[derive(PartialEq, Eq)]
pub enum Side {
    Left,
    Right,
}

pub struct Paddle {
    pub side: Side,
    pub width: f32,
    pub height: f32,
}

impl Paddle {
    fn new(side: Side) -> Paddle {
        Paddle {
            side,
            width: PADDLE_WIDTH,
            height: PADDLE_HEIGHT,
        }
    }
}

impl Component for Paddle {
    type Storage = DenseVecStorage<Self>;
}

fn initialise_paddles(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    let mut left_transform = Transform::default();
    let mut right_transform = Transform::default();

    // Correctly position the paddles.
    let y = MAP_HEIGHT / 2.0;
    left_transform.set_translation_xyz(PADDLE_WIDTH * 0.5, y, 0.0);
    right_transform.set_translation_xyz(MAP_WIDTH - PADDLE_WIDTH * 0.5, y, 0.0);

    let sprite_render = SpriteRender::new(sprite_sheet_handle, 0);

    // Create a left plank entity.
    world
        .create_entity()
        .with(sprite_render.clone())
        .with(Paddle::new(Side::Left))
        .with(left_transform)
        .build();

    // Create right plank entity.
    world
        .create_entity()
        .with(sprite_render)
        .with(Paddle::new(Side::Right))
        .with(right_transform)
        .build();
}

fn load_sprite_sheet(world: &mut World) -> Handle<SpriteSheet> {
    // Load the sprite sheet necessary to render the graphics.
    // The texture is the pixel data
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "assets/textures/pong_spritesheet.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    //...

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "assets/textures/pong_spritesheet.ron", // Here we load the associated ron file
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}

// pub struct Square {
//     pub size: f32,
// }

// impl Square {
//     fn new(size: f32) -> Square {
//         let s = Square {
//             size,
//         };
//         return s;
//     }
// }

// impl Component for Square {
//     type Storage = DenseVecStorage<Self>; // a "storage type" in Amethyst determines how the data will be stored
// }

// fn initialize_squares(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>){
//     let mut trans = Transform::default();

//     let x = MAP_WIDTH / 2.0;
//     let y = MAP_HEIGHT / 2.0;
//     trans.set_translation_xyz(x, y, 0.0);

//     let sprite_render = SpriteRender::new(sprite_sheet_handle, 0);

//     world
//         .create_entity()
//         .with(sprite_render.clone())
//         .with(Square::new(SQUARE_SIZE))
//         .with(trans.clone())
//         .build();
    
//     world
//         .create_entity()
//         .with(sprite_render.clone())
//         .with(Square::new(SQUARE_SIZE))
//         .with(trans)
//         .build();
// }